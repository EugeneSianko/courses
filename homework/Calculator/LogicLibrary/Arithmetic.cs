﻿using System;
using System.Collections.Generic;

namespace BL
{
    public class Arithmetic: IArithmetic
    {
        private Dictionary<string, Func<double, double, double>> _operations =
          new Dictionary<string, Func<double, double, double>>
          {
                { "+", (x, y) =>  x + y },
                { "-", (x, y) => x - y },
                { "*", (x, y) => x * y },
                { "/", (x, y) => x / y },
          };

        public event EventHandler<MyEventArgs> PerformOperationEvent;

        public void AvailableOperation()
        {
            Console.Write("Available operation: ");
            foreach (string key in _operations.Keys)
            {
                Console.Write(key + ' ');
            }
            Console.WriteLine();
        }

        public void PerformOperation(string x, string op, string y)
        {

            if (!_operations.ContainsKey(op))
            throw new ArgumentException(string.Format("Operation {0} is invalid", op));
            OnPerformOperationEvent(new MyEventArgs( _operations[op](Double.Parse(x), Double.Parse(y)).ToString()));

        }

        protected void OnPerformOperationEvent(MyEventArgs e)
        {
            var handler = PerformOperationEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
