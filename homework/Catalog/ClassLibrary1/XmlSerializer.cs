﻿using System.Xml.Serialization;
using System.IO;
using Music;

namespace Serialization
{
    public class XmlSerializerClass<T> : ISerialization<T>
    {
        private XmlSerializer formatter = new XmlSerializer(typeof(T));

        public void Serialaze(T e)
        {
            using (FileStream fs = new FileStream("CompositionList.xml", FileMode.Create))
            {
                formatter.Serialize(fs, e);            
            }
        }
        public T DeSerialaze()
        {
            using (FileStream fs = new FileStream("CompositionList.xml", FileMode.Open))
            {
                T e = (T)formatter.Deserialize(fs);
                return e;
            }
        }
    }
}
