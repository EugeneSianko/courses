﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Music;

namespace Serialization
{
    public interface ISerialization<T>
    {
        void Serialaze(T e);

        T DeSerialaze();
    }
}
