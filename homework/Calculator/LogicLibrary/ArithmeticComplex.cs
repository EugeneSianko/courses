﻿using System;
using System.Collections.Generic;
using ComplexNumbers;
using Parser;

namespace BL
{
    public class ArithmeticComplex: IArithmetic
    {
        private Dictionary<string, Func<Complex, Complex, Complex>> _operations =
          new Dictionary<string, Func<Complex, Complex, Complex>>
          {
                { "+", (x, y) => x + y },
                { "-", (x, y) => x - y },
                { "*", (x, y) => x * y },
                { "/", (x, y) => x / y },
          };


        public void AvailableOperation()
        {
            Console.Write("Available operation: ");
            foreach (string key in _operations.Keys)
            {
                Console.Write(key + ' ');
            }
            Console.WriteLine();
        }


        public void PerformOperation(string x, string op, string y)
        {
            if (!_operations.ContainsKey(op))
                throw new ArgumentException(string.Format("Operation {0} is invalid", op));
            var pars = new ParserComplex();
            OnPerformOperationEvent( new MyEventArgs(_operations[op](pars.parse(x), pars.parse(y)).ToString()));
        }

        public event EventHandler<MyEventArgs> PerformOperationEvent;
        protected void OnPerformOperationEvent(MyEventArgs e)
        {
            var handler = PerformOperationEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
