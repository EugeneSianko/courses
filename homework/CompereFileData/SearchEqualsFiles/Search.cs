﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization;

namespace SearchEqualsFiles
{
    [DataContract(Name = "Equals files",IsReference =false,Namespace ="")]
    public class Search
    { 
        public DirectoryInfo Directory;
   
       [DataMember(Name = "FilteredDictionary")]
        public Dictionary<string, List<string>> FilteredDictionary;
        public Search(string directoryPath)
        {
            Directory = new DirectoryInfo(directoryPath);
            GetAndGroupFileInfosFromDirectory(Directory);
        }

        public override string ToString()
        {
            string Str = "";
            foreach (var pair in FilteredDictionary)
            {
                Str += pair.Key + "\n\n";
                foreach (var obj in pair.Value)
                {
                    Str += obj + "\n";
                }
                Str += "\n\n";
            }
            return Str + "\n\n";
        }


        public void GetAndGroupFileInfosFromDirectory(DirectoryInfo directory)
        {
            Dictionary<long, List<string>> FileInfoDictionary = directory.GetFiles("*.*", SearchOption.AllDirectories).
                GroupBy(x => x.Length).Where(x => x.Count() > 1).OrderBy(x => x.Key).ToDictionary(X => X.Key, X => X.Select(Y=>Y.FullName).
                ToList());

            FilteredDictionary = new Dictionary<string, List<string>>();
            foreach (var pair in FileInfoDictionary)
            {
                int count = 0;
                while (pair.Value.Count > 1)
                {

                    List<string> list = new List<string>();
                    list.Add(pair.Value[0]);
                    for (int i = 1; i < pair.Value.Count; i++)
                    {
                        if (FileCompare(list[0], pair.Value[i]))
                        {
                            list.Add(pair.Value[i]);
                        }
                    }
                    foreach (var file in list)
                    {
                        pair.Value.Remove(file);
                    }
                    if (list.Count > 1) FilteredDictionary.Add(String.Format("Length {1} Group {0} Count {2}", ++count, pair.Key,list.Count), list);
                }

            }
        }



        private bool FileCompare(string file1, string file2)
        {
            int file1byte;
            int file2byte;
            try
            {
                using (var fs1 = new FileStream(file1, FileMode.Open))
                using (var fs2 = new FileStream(file2, FileMode.Open))
                {

                    // Determine if the same file was referenced two times.
                    if (file1 == file2)
                    {
                        // Return true to indicate that the files are the same.
                        return true;
                    }

                    // Check the file sizes. If they are not the same, the files 
                    // are not the same.
                    if (fs1.Length != fs2.Length)
                    {
                        // Return false to indicate files are different
                        return false;
                    }

                    // Read and compare a byte from each file until either a
                    // non-matching set of bytes is found or until the end of
                    // file1 is reached.
                    do
                    {
                        // Read one byte from each file.
                        file1byte = fs1.ReadByte();
                        file2byte = fs2.ReadByte();
                    }
                    while ((file1byte == file2byte) && (file1byte != -1));

                    return ((file1byte - file2byte) == 0);
                }
            }
            catch
            {
                return false;
            }
        
            

        }

    }
}
