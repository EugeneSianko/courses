﻿using System;

namespace ComplexNumbers
{
    public struct Complex: IComparable<Complex>, IEquatable<Complex>
    {

        public double Real { get; set; }
        public double Imaginary { get; set; }

        public double Magnitude
        {
            get { return Math.Sqrt(Math.Pow(Real,2)+Math.Pow(Imaginary,2)); }
        }

        public override string ToString()
        {
            return String.Format("{0} + {1}i", Real, Imaginary);
        }

        public static bool operator ==(Complex a, Complex b)
        {
            return Equals(a, b);
        }

        public static bool operator !=(Complex a, Complex b)
        {
            return !(Equals(a, b));
        }

        public static bool operator <(Complex a, Complex b)
        {
            if (a.CompareTo(b)==-1)
            {
                return true;
            } else return false;
        }

        public static bool operator >(Complex a, Complex b)
        {
            return !(a < b);
        }

        public static bool operator <=(Complex a, Complex b)
        {
            return (Equals(a, b) || a < b);
            
        }

        public static bool operator >=(Complex a, Complex b)
        {
            return (Equals(a, b) || a > b);
        }

        public int CompareTo(Complex other)
        {
            if (other == null) return 1;
            if (other.Magnitude < this.Magnitude) return 1;
            else if (other.Magnitude > this.Magnitude) return -1;
            else return 0;
        }

        public bool Equals(Complex other)
        {
            if (Object.ReferenceEquals(this, other))
            {
                return true;
            }
            if (((object)this == null) || ((object)other == null))
            {
                return false;
            }
                return this.Magnitude == other.Magnitude;
        }


        public static Complex operator +(Complex left, Complex right)
        {
            Complex buffer = new Complex();
            buffer.Real = left.Real + right.Real;
            buffer.Imaginary = left.Imaginary + right.Imaginary;
            return buffer;
        }
        public static Complex operator -(Complex left, Complex right)
        {
            Complex buffer = new Complex(); 
            buffer.Real = left.Real - right.Real;
            buffer.Imaginary = left.Imaginary - right.Imaginary;
            return buffer;
        }
        public static Complex operator *(Complex left, Complex right)
        {
            Complex buffer = new Complex(); 
            buffer.Real = left.Real * right.Real - left.Imaginary * right.Imaginary;
            buffer.Imaginary = left.Real * right.Imaginary + right.Real * left.Imaginary;
            return buffer;
        }
        public static Complex operator /(Complex left, Complex right)
        {
            Complex buffer = new Complex(); 
            buffer.Real = (left.Real * right.Real + left.Imaginary * right.Imaginary) / (Math.Pow(right.Imaginary, 2) + Math.Pow(right.Real, 2));
            buffer.Imaginary = (right.Real * left.Imaginary - left.Real * right.Imaginary) / (Math.Pow(right.Imaginary, 2) + Math.Pow(right.Real, 2));
            return buffer;
        }
    }
}
