﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BL;

namespace WpfCalculator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Arithmetic arith = new Arithmetic();
        public MainWindow()
        {
            InitializeComponent();
            button1DOT.Content = separator.ToString();
            arith.PerformOperationEvent += Arith_PerformOperationEvent;

        }

        private void Arith_PerformOperationEvent(object sender, MyEventArgs e)
        {
            result = Convert.ToDouble(e.Resalt);
            EntryNumber.Content = result;
        }

        private bool isPrevStepsEmpty = true;
        private bool NumOrOperator = false; // true - Operator click last
        private double result = 0, bufResult, M_buffer = 0;
        private bool specialOperator = false;
        private int specialOperatorStringSize;
        private string Math_operator;
        private static readonly char separator = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator[0];
        const string SQRT = "√", SQR = "x²", FRACTION = "1/x", PERCENT = "%";

        private void buttonsNumbers_Click(object sender, RoutedEventArgs e)
        {
            if (NumOrOperator || specialOperator)
            {
                EntryNumber.Content = "";
                NumOrOperator = false;
                if (specialOperator) PrevSteps.Content = PrevSteps.Content.ToString().Substring(0, PrevSteps.Content.ToString().Length - specialOperatorStringSize);
                specialOperator = false;
            }
            if (EntryNumber.Content.Equals("0")) EntryNumber.Content = "";
            if (EntryNumber.Content.ToString().Length < 15)
            {
                Button b = (Button)sender;
                EntryNumber.Content +=  b.Content.ToString();
            }
        }

        private void buttonEquals_Click(object sender, RoutedEventArgs e)
        {
            
            BaseArithmetic(Math_operator, EntryNumber.Content.ToString(), EntryNumber.Content.ToString());
            PrevSteps.Content = "";
            isPrevStepsEmpty = true;
            NumOrOperator = true;
            specialOperator = false;
            result = 0;

        }

        private void buttons_Arthmenic_Click(object sender, RoutedEventArgs e)
        {
            
            Button b = (Button)sender;
            if (isPrevStepsEmpty && !EntryNumber.Content.ToString().Equals("0"))
            {
                PrevSteps.Content += EntryNumber.Content.ToString() + ' ' + b.Content.ToString()[0] + ' ';
                isPrevStepsEmpty = false;
                result = Convert.ToDouble(EntryNumber.Content);
            }
            else if (!isPrevStepsEmpty)
            {
                if (!specialOperator)
                {
                    if (!NumOrOperator)
                    {
                        BaseArithmetic(b.Content.ToString(), EntryNumber.Content.ToString(), EntryNumber.Content.ToString());
                        
                    }
                    else
                    {
                        PrevSteps.Content = PrevSteps.Content.ToString().Substring(0, PrevSteps.Content.ToString().Length - 2);
                        PrevSteps.Content += b.Content.ToString() + ' ';
                    }
                }
                else
                {
                    if (!NumOrOperator)
                    {
                        result = bufResult;
                        PrevSteps.Content += b.Content.ToString() + ' ';
                    }
                    else
                    {
                        PrevSteps.Content += b.Content.ToString() + ' ';
                        BaseArithmetic(b.Content.ToString(), bufResult.ToString(), "");
                        
                    }
                }

            }
            Math_operator = b.Content.ToString();
            NumOrOperator = true;
            specialOperator = false;
        }

        private void BaseArithmetic(string Sign_operator, string s1, string s2)
        {
            arith.PerformOperation( result.ToString(), Math_operator, s1);
            PrevSteps.Content += s2 + ' ' + Sign_operator + ' ';
        }
        
        private void button_erase_Click(object sender, RoutedEventArgs e)
        {
            if (EntryNumber.Content.ToString().Length > 1 && !EntryNumber.Content.ToString().Equals("0"))
                EntryNumber.Content = EntryNumber.Content.ToString().Substring(0, EntryNumber.Content.ToString().Length - 1);
            else EntryNumber.Content = "0";
        }

        private void button_CE_Click(object sender, RoutedEventArgs e)
        {
            EntryNumber.Content = "0";
            NumOrOperator = true;
            result = 0;
            if (specialOperator)
            {
                PrevSteps.Content = PrevSteps.Content.ToString().Substring(0, PrevSteps.Content.ToString().Length - specialOperatorStringSize);
                specialOperator = false;
            }
        }


        private void button_C_Click(object sender, RoutedEventArgs e)
        {
            PrevSteps.Content = "";
            EntryNumber.Content = "0";
            isPrevStepsEmpty = true;
            NumOrOperator = false;
            result = 0;
            specialOperator = false;
        }

        private void button_dot_Click(object sender, RoutedEventArgs e)
        {
            if (!EntryNumber.Content.ToString().Contains(separator))
            {
                if (!specialOperator) EntryNumber.Content += separator.ToString();
                else
                {
                    PrevSteps.Content = PrevSteps.Content.ToString().Substring(0, PrevSteps.Content.ToString().Length - specialOperatorStringSize);
                    specialOperator = false;
                    EntryNumber.Content = "0" + separator;
                }
            }

        }

        private void button_SwapSign_Click(object sender, RoutedEventArgs e)
        {
            EntryNumber.Content = (-1 * Convert.ToDouble(EntryNumber.Content)).ToString();
            if (specialOperator)
            {
                string buf = PrevSteps.Content.ToString().Substring(PrevSteps.Content.ToString().Length - specialOperatorStringSize, PrevSteps.Content.ToString().Length);
                PrevSteps.Content = PrevSteps.Content.ToString().Substring(0, PrevSteps.Content.ToString().Length - specialOperatorStringSize);
                PrevSteps.Content += System.String.Format("negative({0})", buf);
                specialOperatorStringSize += 10;
                bufResult = -bufResult;
            }
        }
        private void button_M_Click(object sender, RoutedEventArgs e)
        {
            Button b = (Button)sender;
            switch (b.Content.ToString())
            {
                case "MR":
                    if (NumOrOperator) { EntryNumber.Content = ""; NumOrOperator = false; }
                    if (EntryNumber.Content.ToString().Equals("0")) { EntryNumber.Content = ""; }
                    EntryNumber.Content += M_buffer.ToString();
                    break;

                case "MC":
                    buttonMR.IsEnabled = false;
                    buttonMC.IsEnabled = false;
                    M_buffer = 0;
                    break;
                default:
                    if (!EntryNumber.Content.ToString().Equals("0"))
                    {
                        NumOrOperator = true;
                        buttonMR.IsEnabled = true;
                        buttonMC.IsEnabled = false;
                        switch (b.Content.ToString())
                        {
                            case "M+":
                                M_buffer += Convert.ToDouble(EntryNumber.Content);
                                break;
                            case "M-":
                                M_buffer -= Convert.ToDouble(EntryNumber.Content);
                                break;
                            case "MS":
                                M_buffer = Convert.ToDouble(EntryNumber.Content);
                                break;
                        }
                    }
                    break;
            }
        }
    }
}
