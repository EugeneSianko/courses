﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Music;

namespace Serialization
{
    public class BinarySerialaze<T>: ISerialization<T>
    {
        private BinaryFormatter formatter = new BinaryFormatter();
        public void Serialaze(T e)
        {
            using (FileStream fs = new FileStream("CompositionList.dat", FileMode.Create))
            {
                formatter.Serialize(fs, e);
            }
        }

        public T DeSerialaze()
        {
            using (FileStream fs = new FileStream("CompositionList.dat", FileMode.Open))
            {
                T e = (T)formatter.Deserialize(fs);
                return e;
            }
        }
    }
}
