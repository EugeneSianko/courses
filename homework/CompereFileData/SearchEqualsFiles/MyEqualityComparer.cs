﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SearchEqualsFiles
{
    public class MyEqualityComparer : IEqualityComparer<string>
    {
        public bool Equals(string file1, string file2)
        {
            int file1byte;
            int file2byte;
            using (var fs1 = new FileStream(file1, FileMode.Open))
            using (var fs2 = new FileStream(file2, FileMode.Open))
            {

                // Determine if the same file was referenced two times.
                if (file1 == file2)
                {
                    // Return true to indicate that the files are the same.
                    return true;
                }

                // Check the file sizes. If they are not the same, the files 
                // are not the same.
                if (fs1.Length != fs2.Length)
                {
                    // Return false to indicate files are different
                    return false;
                }

                // Read and compare a byte from each file until either a
                // non-matching set of bytes is found or until the end of
                // file1 is reached.
                do
                {
                    // Read one byte from each file.
                    file1byte = fs1.ReadByte();
                    file2byte = fs2.ReadByte();
                }
                while ((file1byte == file2byte) && (file1byte != -1));

                return ((file1byte - file2byte) == 0);
            }
        }       

        public int GetHashCode(string obj)
        {
            return 0;
        }

    }
}
