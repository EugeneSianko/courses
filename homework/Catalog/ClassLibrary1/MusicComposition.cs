﻿using System;

namespace Music
{
    [Serializable]
    public struct MusicComposition
    {
        public string Title { set; get; }
        public string Author { set; get; }
        public string Year { set; get; }
        public string Album { set; get; }

        public override string ToString()
        {
            return String.Format("{0} - {1} - {2} - {3}", Title, Author, Album, Year);
        }

        public MusicComposition(string[] row)
        {
            Title = row[0];
            Author = row[1];
            Album = row[2];
            Year = row[3];
        }

        public MusicComposition(string Title, string Author, string Album, string Year)
        {
            this.Title = Title;
            this.Author = Author;
            this.Album = Album;
            this.Year = Year;
        }

    }
}
