﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    [Serializable]
    public class MyEventArgs : EventArgs
    {
        public string Resalt { get; set; }

        public MyEventArgs(string Resalt)
        {
            this.Resalt = Resalt;
        }
        public MyEventArgs()
        {
        }
    }
}
