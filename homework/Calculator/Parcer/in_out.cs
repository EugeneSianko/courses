﻿using System;

namespace Input
{
    public class InputClass
    {

        public static string get_first()
        {
            Console.WriteLine("Enter the first number");
            return Console.ReadLine();
        }

        public static string get_second()
        {
            Console.WriteLine("Enter the second number");
            return Console.ReadLine();
        }

        public static string get_op()
        {            
            Console.WriteLine("Enter operator");
            return Console.ReadLine();            
        }
    }
}
