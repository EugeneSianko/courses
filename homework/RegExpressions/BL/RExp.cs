﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Text.RegularExpressions;

namespace BL
{
    public class RExp
    {

        public Dictionary<double, string> patterns = new Dictionary<double, string>
        {
            { 1,@"[^ \n]+"},
            { 2,@"a[^ \n]*a"},
            { 3,@"\b[^aeiou]{2,4}\w*\s"},
            { 4,@"[0-9]+,[0-9]*"},
            { 5,@"\w+\.\w+"},
            { 6,@"http://[^ \n]*"},
            { 7.1,@"(^+|\n+)+[^ \n]+"},
            { 7.2,@"[^ \n]+($+|\n+)"},
            { 7.3,@"(^+|\n+)+( )*[^ \n]+"},
            { 7.4,@"(^+|\n+)+( )+[^ \n]+"},
        };
        public ArrayList perfomOperations(string source,double number)
        {
            ArrayList result = new ArrayList();

            Regex reg = new Regex(patterns[number]);
            MatchCollection matches = reg.Matches(source);
            foreach(Match match in matches)
            {
                result.Add(Regex.Replace(match.ToString(), @"\n", ""));
            }

            return result;
        }


        public void Seven1(string source)
        {
            Regex reg = new Regex(@"(^+|\n+)+[^ \n]+");
            MatchCollection matches = reg.Matches(source);
            foreach (Match match in matches)
            {
                Console.WriteLine(Regex.Replace(match.ToString(), @"\n| ", ""));
            }

        }



    }
}
