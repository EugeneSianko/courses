﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL;

namespace RegExpressions
{
    class Manager
    {
        static void Main(string[] args)
        {

            string source = TextFileToString.Get();

            RExp rexp = new RExp();
            IO io = new IO();

            while (true)
            {

                    Console.WriteLine("\nSource:\n");
                    Console.WriteLine(source);

                    Console.Write("\nChoice pattern:\n");
                    double choice = Convert.ToDouble(Console.ReadLine());
                    ArrayList result = rexp.perfomOperations(source, choice);
                    foreach (string e in result)
                    {
                        Console.WriteLine(e);
                    }

            }

            
        }
    }
}
