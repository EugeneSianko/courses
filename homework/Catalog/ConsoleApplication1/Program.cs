﻿using Music;
using Serialization;
using System;
using ClassLibrary1;


namespace ConsoleManager
{
    class Program
    {
        static CompositionList _list;
        static bool CanChange;

        static void Main(string[] args)
        {
            while (true)
            {
                try
                {
                    if (_list == null)
                    {

                        _list = new CompositionList();
                        _list =_list.load();
                        CanChange = true;
                        _list.BeforeChangeEvent += E_BeforeChangeEvent;
                        _list.AfterChangeEvent += E_AfterChangeEvent;
                    }

                    ShowList();

                    Console.WriteLine("{0}\n", Properties.Resources.OperationList);
                    int choice = Convert.ToInt16(Console.ReadLine());

                    switch (choice)
                    {
                        case 1:
                            ShowList();
                            break;
                        case 2:
                           _list.save(_list);
                            Console.WriteLine("List have saved");
                            break;
                        case 3:
                            Find();
                            break;
                        case 4:
                            SortList();
                            break;                           
                        case 5:
                            AddItem();
                            break;
                        case 6:
                            break;
                        case 7:
                            break;
                        default:
                            Console.WriteLine("Incorect input");
                            break;
                    }                  
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    Console.ReadLine();
                }
            }
        }

        private static void AddItem()
        {
            
        }

        static void ShowList()
        {
            Console.WriteLine("{0}\n", _list.ToString());
        }

        private static void SortList()
        {
            Console.WriteLine("Chose Field");
            int choise = Convert.ToInt16(Console.ReadLine());
            _list.Sort(choise);
        }

        private static void Find()
        {
            Console.WriteLine("Chose Field No");
            int choise = Convert.ToInt16(Console.ReadLine());
            Console.WriteLine("Enter Search Word");
            string searchWord= Console.ReadLine();
            Console.WriteLine(_list.FindAll(searchWord, choise));
        }



        static void E_BeforeChangeEvent(object sender, MyEventArgs e)
        {
            e.CanChange = CanChange;
        }

        private static void E_AfterChangeEvent(object sender, MyEventArgs e)
        {
            Console.Clear();
            Console.WriteLine(e.Message);
            ShowList();
        }
    }
}
