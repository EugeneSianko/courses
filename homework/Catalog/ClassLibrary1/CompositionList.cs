﻿using System;
using System.Collections.Generic;
using System.Linq;
using ClassLibrary1;
using Serialization;
using System.Xml.Serialization;



namespace Music
{
    [Serializable]
    public class CompositionList: BaseListCover
    {
        [XmlIgnore]
        public Serialaze<CompositionList> save;
        [XmlIgnore]
        public DeSerialaze<CompositionList> load;
        [XmlIgnore]
        public ISerialization<CompositionList> _Serialization;

        private List<MusicComposition> _compositionList;
        public List<MusicComposition> compositionList
        {
            get { return _compositionList; }
            set { _compositionList = value; }
        }



        public CompositionList()
        {
            _compositionList = new List<MusicComposition>();

            _Serialization = new XmlSerializerClass<CompositionList>();
            save = new Serialaze<CompositionList>(_Serialization.Serialaze);
            load = new DeSerialaze<CompositionList>(_Serialization.DeSerialaze);
        }

        public CompositionList(ISerialization<CompositionList> Serialization)
        {
            _compositionList = new List<MusicComposition>();
            this._Serialization = Serialization;
            save = new Serialaze<CompositionList>(_Serialization.Serialaze);
            load = new DeSerialaze<CompositionList>(_Serialization.DeSerialaze);
        }
        public CompositionList(List<MusicComposition> musicComposition)
        { 
            _compositionList = new List<MusicComposition>();
            _compositionList.AddRange(musicComposition);
        }

        public override string ToString()
        {
            string _string = "";
            foreach (MusicComposition str in _compositionList)
                _string += str.ToString();
            return _string;
        }

        private Dictionary<int, Func<MusicComposition, string>> _fields =
        new Dictionary<int, Func<MusicComposition, string>>
        {
                { 0, x => x.Title},
                { 1, x => x.Author},
                { 2, x => x.Album},
                { 3, x => x.Year},
        };

       public CompositionList FindAll(string ToFind, int _indexOfField = 0)
        {
            return new CompositionList(_compositionList.FindAll(x => _fields[_indexOfField](x) == ToFind));
        }



        public void Sort(int _indexOfField)
        {
            MyEventArgs eventArgs = new MyEventArgs(true, String.Format("List have sort by {0} field", _indexOfField));
            OnBeforeChangeEvent(eventArgs);
            if (eventArgs.CanChange)
            {
                _compositionList = _compositionList.OrderBy(_fields[_indexOfField]).ToList();
                OnAfterChangeEvent(eventArgs);
            }
        }
        public void Add(MusicComposition _composition)
        {
            MyEventArgs eventArgs = new MyEventArgs(true, "Add element in end");
            OnBeforeChangeEvent(eventArgs);
            if (eventArgs.CanChange)
            {
                _compositionList.Add(_composition);
                OnAfterChangeEvent(eventArgs);
            }
        }

        public void RemoveAt(int index)
        {
            MyEventArgs eventArgs = new MyEventArgs(true, String.Format("Remove element {0}", index));
            OnBeforeChangeEvent(eventArgs);
            if (eventArgs.CanChange)
            {
                _compositionList.RemoveAt(index);
                OnAfterChangeEvent(eventArgs);

            }
        }

        public void ReplaceAt(int index, MusicComposition _newMC)
        {
            MyEventArgs eventArgs = new MyEventArgs(true, String.Format("Replace at {0}", index));
            OnBeforeChangeEvent(eventArgs);
            if (eventArgs.CanChange)
            {
                _compositionList[index] = _newMC;
                OnAfterChangeEvent(eventArgs);
            }
        }

        public void Reverse()
        {
            MyEventArgs eventArgs = new MyEventArgs(true, "Reverse");
            OnBeforeChangeEvent(eventArgs);
            if (eventArgs.CanChange)
            {
                _compositionList.Reverse();
                OnAfterChangeEvent(eventArgs);
            }
        }

        public event EventHandler<MyEventArgs> BeforeChangeEvent;
        public event EventHandler<MyEventArgs> AfterChangeEvent;

        protected void OnBeforeChangeEvent(MyEventArgs e)
        {
            var handler = BeforeChangeEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        protected void OnAfterChangeEvent(MyEventArgs e)
        {
            var handler = AfterChangeEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
