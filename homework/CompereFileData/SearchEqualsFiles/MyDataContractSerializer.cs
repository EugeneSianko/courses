﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;


namespace SearchEqualsFiles
{
    public class MyDataContractSerializer
    {

        public static void Save<T>(T _object, string fileName)
        {

            var serializer = new DataContractSerializer(typeof(T));
            string xmlString;
            using (FileStream xml = new FileStream(String.Format("{0}.xml", fileName), FileMode.Create))
            {
                serializer.WriteObject(xml, _object);
            }

            using (StreamWriter txt = new StreamWriter((String.Format("{0}.txt", fileName))))
            {
                txt.WriteLine(_object.ToString());
            }
        }
    }
}

