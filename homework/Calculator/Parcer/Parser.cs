﻿using System;
using ComplexNumbers;

namespace Parser
{
    public class ParserComplex
    {
        public Complex parse(string row)
        {
            string[] arr = row.Split('+', '-');
            Complex result = new Complex();
            result.Real = Convert.ToDouble(arr[0]);
            result.Imaginary = Convert.ToDouble(arr[1]);
            return result;
        }
    }
}
