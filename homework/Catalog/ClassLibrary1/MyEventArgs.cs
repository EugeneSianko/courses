﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    [Serializable]
    public class MyEventArgs:EventArgs
    {
        public bool CanChange{ get; set; }
        public string Message { get; set; }

        public MyEventArgs(bool _CanChange, string Message)
        {
            CanChange = _CanChange;
            this.Message = Message;
        }
        public MyEventArgs()
        {
            CanChange = true;
        }
        public MyEventArgs(bool CanChange)
        {
            this.CanChange = CanChange;
        }

    }
}
