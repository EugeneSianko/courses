﻿using System;
using BL;
using Input;

namespace MyProgram
{
    class Program
    {

        static void Main(string[] args)
        {
            
            while (true)
            {
                
                try
                {
                    IArithmetic Arith;
                    Console.WriteLine("Select with what numeric1s you will work: /n {0} - simple; {1} - complex");
                    int i = Int32.Parse(Console.ReadLine());
                    if (i == 1)
                    {
                        Arith = (IArithmetic)new ArithmeticComplex();

                    }
                    else
                    {
                        Arith = (IArithmetic)new Arithmetic();

                    }

                    Arith.PerformOperation(InputClass.get_first(), InputClass.get_op(), InputClass.get_second());


                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (FormatException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (OverflowException e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    Console.ReadLine();
                }           
            }

        }
    }
}
