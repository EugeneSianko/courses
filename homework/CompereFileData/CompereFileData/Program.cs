﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SearchEqualsFiles;
using System.Diagnostics;


namespace CompereFileData
{
    class Program
    {
        static void Main(string[] args)
        {
            var directory = Console.ReadLine();
            var watch = Stopwatch.StartNew(); 
            Search RF = new Search(directory);
            watch.Stop();            
            Console.WriteLine(watch.ElapsedMilliseconds);
            MyDataContractSerializer.Save(RF, RF.Directory.Name);
            Console.ReadLine();

        }
    }
}
