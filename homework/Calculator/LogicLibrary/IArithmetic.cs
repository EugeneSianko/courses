﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public interface IArithmetic
    {
        void PerformOperation(string x, string op, string y);
        void AvailableOperation();

        event EventHandler<MyEventArgs> PerformOperationEvent;
    }
}
